package main

type User struct {
	Email     string `json:"email" validate:"required"`
	Pass      string `json:"pass" validate:"required"`
	Birthdate string `json:"birthdate" validate:"required"`
}

type Users map[string]User

type Database struct {
	Users Users
}

func (d *Database) AddUser(user User) {
	d.Users[user.Email] = user
}

func (d *Database) LoginUser(email, pass string) bool {
	if dest, ok := d.Users[email]; ok {
		return dest.Pass == pass
	}
	return false
}
