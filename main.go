package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type LoginForm struct {
	Email string `json:"email" validate:"required"`
	Pass  string `json:"pass" validate:"required"`
}

var DB = &Database{
	Users: make(Users),
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/register", RegisterHandler).
		Methods("POST").
		Headers("Content-Type", "application/json")

	r.HandleFunc("/login", LoginHandler).
		Methods("POST").
		Headers("Content-Type", "application/json")

	r.MethodNotAllowedHandler = http.HandlerFunc(NotAllowedHandler)
	r.NotFoundHandler = http.HandlerFunc(NotFoundHandler)

	server := &http.Server{
		Addr:    "localhost:8080",
		Handler: r,
	}

	log.Fatal(server.ListenAndServe())
}
