package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gopkg.in/go-playground/validator.v9"
)

func checkValidHeader(header http.Header, k, v string) bool {
	if dest, ok := header[k]; ok {
		for _, h := range dest {
			if h == v {
				return true
			}
		}
	}
	return false
}

func checkValidBody(data interface{}) bool {
	validate := validator.New()
	if errs := validate.Struct(data); errs != nil {
		log.Printf("%v", errs)
		return false
	}
	return true
}

func NotFoundHandler(w http.ResponseWriter, r *http.Request) {
	if !checkValidHeader(r.Header, "Content-Type", "application/json") {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid header"))
		return
	}
}

func NotAllowedHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusMethodNotAllowed)
}

func RegisterHandler(w http.ResponseWriter, r *http.Request) {
	h := w.Header()
	h.Set("Content-Type", "application/json")

	var data User
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil || !checkValidBody(data) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(`{"message": "invalid body"}`))
		return
	}
	// Adicionar user ao banco
	DB.AddUser(data)
	fmt.Fprint(w, `{"message": "user successfully registered"}`)
}

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	h := w.Header()
	h.Set("Content-Type", "application/json")

	var data LoginForm
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil || !checkValidBody(data) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(`{"message": "invalid body"}`))
		return
	}

	// Verifica login
	if DB.LoginUser(data.Email, data.Pass) {
		fmt.Fprint(w, `{"message": "user successfully login"}`)
	} else {
		// Caso o usuário e senha não correspondam as usuários cadastrados: Deve ser respondido o código 403 e o body em branco
		w.WriteHeader(http.StatusForbidden)
		return
	}
}
