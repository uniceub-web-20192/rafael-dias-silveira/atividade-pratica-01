module gitlab.com/s-rafaeldias/atividade-pratica-01

go 1.12

require (
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/gorilla/mux v1.7.3
	github.com/leodido/go-urn v1.1.0 // indirect
	gopkg.in/go-playground/validator.v9 v9.29.1
	gopkg.in/validator.v2 v2.0.0-20190827175613-1a84e0480e5b
)
